FreeBody Visualiser Mobile Android Plugin
-----------------------------------------

Launches the Android system UI file picker, and un-packages selected FreeBody model package files.

Development Notes
-----------------

Developed using [Android Studio][android_studio].

For access to base Unity Android player classes, a copy of the
`%UNITY_EDITOR_DATA%/PlaybackEngines/androidplayer/development/bin/classes.jar` file is included
under `/app/libs`.

Refer to the [Unity documentation][unity_android_plugins_doc] for details.

To export for use as Unity plugin, use the `makeJar` task in `/app/build.gradle`.
Then grab the jar file from `/app/release/` and add it to your Unity project's
`/Assets/Plugins/Android/` directory.

Additionally, a copy of the `/app/release/AndroidManifest.xml` must also be placed in the
`/Assets/Plugins/Android/` directory. Note that the `/app/res/drawable/app_icon.png` file
is simply a placeholder and is replaced during Unity's build process.

When overriding the default `UnityPlayerActivity`, as `MainActivity` is doing here, its package
(i.e. `uk.org.msksoftware.freebodyvis`) must match that set in Unity's
`Build Settings > Android > Player Settings > Other Settings > Identification > Bundle Identifier`.

A final note is that the application's resources `library/res/` directory is not included.
Resources like the app icon and name are injected by Unity.

[android_studio]: http://developer.android.com/sdk/index.html
[unity_android_plugins_doc]: http://docs.unity3d.com/Manual/PluginsForAndroid.html