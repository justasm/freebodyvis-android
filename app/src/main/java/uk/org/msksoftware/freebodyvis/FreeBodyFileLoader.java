package uk.org.msksoftware.freebodyvis;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Loads FreeBody packages.
 *
 * Created by Justas on 13/07/2015.
 */
public class FreeBodyFileLoader {
    private static final String LOG_TAG = FreeBodyFileLoader.class.getSimpleName();

    /** Creates an intent to open up a file picker. */
    public static Intent getPickerIntent(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT); // use system's file browser

        // Filter to only show results that can be "opened", such as files
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Search for all documents available via installed storage providers.
        intent.setType("*/*");

        return intent;
    }

    /**
     * @return a reference to the parameter XML file.
     */
    public static File unzipSelectedPackage(Context context, Intent resultData, File location) {
        if (null == resultData) return null;

        // Retrieve the user-selected URI
        Uri uri = resultData.getData();
        Log.i(LOG_TAG, "Uri: " + uri.toString());

        File file = null;
        try {
            InputStream in = context.getContentResolver().openInputStream(uri);
            unzip(in, location);
            file = new File(location, "parameters.xml");
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "File not found for " + uri.getPath(), e);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to read / write.", e);
        }

        return file;
    }

    private static void unzip(InputStream in, File location) throws IOException {
        try (ZipInputStream zin = new ZipInputStream(new BufferedInputStream(in))) {
            ZipEntry entry;

            while ((entry = zin.getNextEntry()) != null) {
                File destination = new File(location, entry.getName());
                Log.i(LOG_TAG, "Unzipping " + entry.getName() +
                        " to " + destination.getAbsolutePath());

                if (entry.isDirectory()) {
                    //noinspection ResultOfMethodCallIgnored
                    destination.mkdirs();
                    continue;
                } else {
                    File dir = destination.getParentFile();
                    if (null != dir) {
                        //noinspection ResultOfMethodCallIgnored
                        dir.mkdirs();
                    }
                }

                FileOutputStream out = new FileOutputStream(destination);
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = zin.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
                out.close();
                zin.closeEntry();
            }
        }
    }

    public static boolean delete(File file){
        if(file.isDirectory()){
            File[] children = file.listFiles();
            for (File child : children) {
                delete(child);
            }
        }
        return file.delete();
    }
}
