package uk.org.msksoftware.freebodyvis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.unity.GoogleUnityActivity;
import com.unity3d.player.UnityPlayer;

import java.io.File;

public class MainActivity extends GoogleUnityActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final String SUB_DIR = "out";

    private static final int PICK_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "Cleaning up any previously unzipped files.");
        FreeBodyFileLoader.delete(new File(getFilesDir(), SUB_DIR));
    }

    /** Note - called from Unity UI code, as of latest update this was UIController.cs */
    public void launchFilePicker() {
        Log.d(LOG_TAG, "Launching file picker.");
        startActivityForResult(FreeBodyFileLoader.getPickerIntent(), PICK_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // Make sure we're responding to the correct request, and that it completed successfully
        if (requestCode == PICK_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Log.d(LOG_TAG, "Unzipping FreeBody file package.");
            File parameterFile = FreeBodyFileLoader.unzipSelectedPackage(this, resultData, new File(getFilesDir(), SUB_DIR));
            UnityPlayer.UnitySendMessage("Controller", "LoadParameterFile", parameterFile.getAbsolutePath());
        }
    }
}
