package uk.org.msksoftware.freebodyvis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.File;

public class TestActivity extends Activity {

    private static final int REQUEST_CODE = 1;
    private static final String SUB_DIR = "out";

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(FreeBodyFileLoader.getPickerIntent(), REQUEST_CODE);
            }
        });
        textView = (TextView) findViewById(R.id.text);

        FreeBodyFileLoader.delete(new File(getFilesDir(), SUB_DIR));
        updateUi();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            FreeBodyFileLoader.unzipSelectedPackage(this, resultData, new File(getFilesDir(), SUB_DIR));
        }

        updateUi();
    }

    private void updateUi(){
        StringBuilder builder = new StringBuilder();
        traversePrint(getFilesDir(), 0, builder);

        textView.setText((builder.toString()));
        builder.setLength(0);
    }

    private void traversePrint(File root, int level, StringBuilder builder){
        for(int i = 0; i < level; i++) builder.append("- ");
        builder.append(root.getName());
        builder.append("\n");

        if(root.isDirectory()){
            File[] children = root.listFiles();
            for (File child : children) {
                traversePrint(child, level + 1, builder);
            }
        }
    }
}
